#ifndef SQUARE_H
#define SQUARE_H

#include <QColor>
#include <QRect>

#include <vector>

using namespace std;

class Square
{
	enum SquarePosition {
		TopLeftCorner,
		TopRightCorner,
		BottomLeftCorner,
		BottomRightCorner,
		FirstRow,
		FirstColumn,
		LastRow,
		LastColumn,
		Normal
	};

	int xmax = 30;
	int ymax = 60;

	void figure_out_neighbor_squares();
	int different_square_cases(int index);
	void get_row_column(int index, int &row, int &column);

public:
	QRect rect;
	int size;
	int index;
	bool is_alive;
	QColor color;
	vector<int> neighbor_indices;
	Square();
	Square(Square* p_other);
	Square(QRect p_rect, int p_size, int p_index, bool p_alive);

	void set_is_alive(bool p_alive);
};

#endif // SQUARE_H
