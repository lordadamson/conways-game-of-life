#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui/QOpenGLFunctions>

#include <QtWidgets>
#include "canvas.h"

QT_BEGIN_NAMESPACE
class QPainter;
class QOpenGLContext;
class QOpenGLPaintDevice;
QT_END_NAMESPACE

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow, protected QOpenGLFunctions
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void on_play_triggered();
	void on_pause_triggered();
	void on_remove_triggered();
	void on_add_triggered();

private:
	Ui::MainWindow *ui;
	Canvas* canvas;
	//QOpenGLWidget* openGLWidget;
};

#endif // MAINWINDOW_H
