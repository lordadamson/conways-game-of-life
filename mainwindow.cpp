#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	canvas = new Canvas();
	ui->grid_layout->addWidget(canvas);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_play_triggered()
{
	canvas->set_simulation(true);
}

void MainWindow::on_pause_triggered()
{
	canvas->set_simulation(false);
}

void MainWindow::on_remove_triggered()
{
	canvas->adding_cells = false;
}

void MainWindow::on_add_triggered()
{
	canvas->adding_cells = true;
}
