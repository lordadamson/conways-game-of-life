#ifndef CANVAS_H
#define CANVAS_H

#include <QWidget>
#include <QBrush>
#include <QMouseEvent>
#include <QImage>
#include <QtWidgets>

#include <vector>
#include <queue>

#include "square.h"

using namespace std;

class Canvas : public QOpenGLWidget
{
	Q_OBJECT

public:
	Canvas(QOpenGLWidget *parent = 0);

	QSize minimumSizeHint() const Q_DECL_OVERRIDE;
	QSize sizeHint() const Q_DECL_OVERRIDE;
	void setBrush(const QBrush &brush);

	bool adding_cells = true;
	void set_simulation(bool value);

protected:
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

private:
	QImage image;
	QBrush brush;
	const int canvas_size_x = 1800;
	const int canvas_size_y = 900;
	vector<Square*> grid;
	vector<Square*> old_grid;
	int square_size = 30;
	bool mouse_pressed = false;
	int old_index = 0;
	int xmax = 30;
	int ymax = 1770;
	queue<int> operations;
	QTimer *timer;
	bool simulating = false;
	bool simulating_mouse_click_temp = false;

	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void init_grid();
	int get_index(int x, int y);
	void modify_grid_and_draw_image(int index);
	vector<Square*> copy_grid();
	void update_square(Square* square);
	int get_number_of_living_neighbours(Square* square);
	void empty_old_grid();

public slots:
	void simulate();
};

#endif // CANVAS_H
