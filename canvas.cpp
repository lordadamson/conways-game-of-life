#include "canvas.h"

#include <QPainter>
#include <QImage>
#include <QtMath>
#include <iostream>
#include <thread>

using namespace std;

Canvas::Canvas(QOpenGLWidget *parent) : QOpenGLWidget(parent)
{
	setBackgroundRole(QPalette::Base);
	setAutoFillBackground(true);
	setFixedSize(canvas_size_x, canvas_size_y);

	init_grid();

	image = QImage(width(), height(), QImage::Format_RGB888);
	QPainter painter(&image);
	painter.setBrush(Qt::green);
	painter.setPen(Qt::NoPen);

	for (Square* square : grid)  // <-- return a reference on each element.
		painter.drawRect(square->rect);

	//connect(this, &Canvas::square_temperature_changed, this, &Canvas::_on_square_temperature_changed);
	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(simulate()));
	timer->start(100);
}

void Canvas::init_grid()
{
	// initialize grid and fill with squares
	int index = 0;
	for (int i = 0; i < width(); i += square_size)
		for(int j = 0; j < height(); j += square_size, index++)
			grid.push_back(
				new Square(QRect(i, j, square_size, square_size), square_size, index, false)
						  );
}

QSize Canvas::minimumSizeHint() const
{
	return QSize(100, 100);
}

QSize Canvas::sizeHint() const
{
	return QSize(400, 200);
}

void Canvas::setBrush(const QBrush &brush)
{
	this->brush = brush;
	update();
}

void Canvas::set_simulation(bool value)
{
	simulating = value;
	if(simulating)
		simulate();
}

// is called every screen update
void Canvas::paintEvent(QPaintEvent * event)
{
	QPainter painter(this);
	QRect event_rect = event->rect();
	painter.drawPixmap(event_rect, QPixmap::fromImage(image));
}

// returns the index of the square on which the cursor stands
int Canvas::get_index(int x, int y)
{
	int index = 0;
	x = x / square_size;
	y = y / square_size;
	index = square_size*x + y;
	return index;
}

void Canvas::modify_grid_and_draw_image(int index)
{
	if (index >= grid.size())
		return;

	grid[index]->set_is_alive(adding_cells);

	QPainter painter(&image);
	painter.setBrush(grid[index]->color);
	painter.setPen(Qt::NoPen);
	painter.drawRect(grid[index]->rect);
	update();
}

vector<Square *> Canvas::copy_grid()
{
	vector<Square*> copied_grid;

	for (Square* square : grid)
		copied_grid.push_back(new Square(square)); // you're pushing pointers to the same objects you dumb piece of shit

	return copied_grid;
}

void Canvas::update_square(Square* square)
{
	int number_of_living_neighbours = get_number_of_living_neighbours(square);

	if (number_of_living_neighbours < 2)
		square->set_is_alive(false);
	else if (number_of_living_neighbours == 3)
		square->set_is_alive(true);
	else if (number_of_living_neighbours > 3)
		square->set_is_alive(false);
}

int Canvas::get_number_of_living_neighbours(Square* square)
{
	int number_of_living_neighbours = 0;

	for (int neighbour_index : square->neighbor_indices)
		if (old_grid[neighbour_index]->is_alive)
			number_of_living_neighbours++;

	return number_of_living_neighbours;
}

void Canvas::empty_old_grid()
{
	for (Square* square : old_grid)
		if (square)
			delete square;
}

void Canvas::simulate()
{
	empty_old_grid();
	old_grid = copy_grid();
	for (Square* square : grid)
	{
		if(!simulating)
			break;

		update_square(square);

		QPainter painter(&image);
		painter.setBrush(square->color);
		painter.setPen(Qt::NoPen);
		painter.drawRect(square->rect);
		update();
	}
}

void Canvas::mousePressEvent(QMouseEvent *event)
{
	simulating_mouse_click_temp = simulating;
	set_simulation(false);
//	if(simulating)
//		return;
	mouse_pressed = true;
	int index = get_index(event->x(), event->y());
	old_index = index;
	modify_grid_and_draw_image(index);
}

void Canvas::mouseMoveEvent(QMouseEvent *event)
{
	if(simulating)
		return;
	int index = get_index(event->x(), event->y());
	if (old_index == index)
		return;
	modify_grid_and_draw_image(index);
}

void Canvas::mouseReleaseEvent(QMouseEvent *event)
{
//	if(simulating)
//		return;
	int index = get_index(event->x(), event->y());
	old_index = index;
	modify_grid_and_draw_image(index);
	mouse_pressed = false;
	set_simulation(simulating_mouse_click_temp);
}

