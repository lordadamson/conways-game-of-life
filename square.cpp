#include "square.h"
#include <iostream>

Square::Square()
{

}

Square::Square(Square* p_other)
{
	rect = p_other->rect;
	size = p_other->size;
	index = p_other->index;
	set_is_alive(p_other->is_alive);
	figure_out_neighbor_squares();
}

Square::Square(QRect p_rect, int p_square_size, int p_index, bool p_is_alive)
{
	rect = p_rect;
	size = p_square_size;
	index = p_index;
	set_is_alive(p_is_alive);
	figure_out_neighbor_squares();
}

void Square::set_is_alive(bool p_is_alive)
{
	is_alive = p_is_alive;

	if (is_alive)
		color = Qt::black;
	else
		color = Qt::green;
}

int Square::different_square_cases(int index)
{
	int row, column;
	get_row_column(index, row, column);

	if (column == 0 && row == 0)
		return SquarePosition::TopLeftCorner;
	if (column == 0 && row == xmax - 1)
		return SquarePosition::BottomLeftCorner;
	if (column == ymax - 1 && row == 0)
		return SquarePosition::TopRightCorner;
	if (column == ymax - 1 && row == xmax - 1)
		return SquarePosition::BottomRightCorner;
	if (column == 0)
		return SquarePosition::FirstColumn;
	if (column == ymax - 1)
		return SquarePosition::LastColumn;
	if (row == 0)
		return SquarePosition::FirstRow;
	if (row == xmax - 1)
		return SquarePosition::LastRow;
	return SquarePosition::Normal;
}

void Square::get_row_column(int index, int &row, int &column)
{
	column = (int)((float)(index)/(float)xmax);
	row = index - column*xmax;
}

void Square::figure_out_neighbor_squares()
{
	int my_neighbors[] = {
		index - (int)xmax - 1,  index - 1,  index + (int)xmax - 1,
		index - (int)xmax,					index + (int)xmax,
		index - (int)xmax + 1,  index + 1,  index + (int)xmax + 1
	};

	int condition = different_square_cases(index);

	if (condition == SquarePosition::Normal)
		neighbor_indices = vector<int>(my_neighbors, my_neighbors+8);
	if (condition == SquarePosition::TopLeftCorner)
		neighbor_indices = {my_neighbors[4], my_neighbors[6], my_neighbors[7]};
	if (condition == SquarePosition::BottomLeftCorner)
		neighbor_indices = {my_neighbors[1], my_neighbors[2], my_neighbors[4]};
	if (condition == SquarePosition::TopRightCorner)
		neighbor_indices = {my_neighbors[3], my_neighbors[5], my_neighbors[6]};
	if (condition == SquarePosition::BottomRightCorner)
		neighbor_indices = {my_neighbors[0], my_neighbors[1], my_neighbors[3]};
	if (condition == SquarePosition::FirstColumn)
		neighbor_indices = {my_neighbors[1], my_neighbors[2], my_neighbors[4], my_neighbors[6], my_neighbors[7]};
	if (condition == SquarePosition::LastColumn)
		neighbor_indices = {my_neighbors[0], my_neighbors[1], my_neighbors[3], my_neighbors[5], my_neighbors[6]};
	if (condition == SquarePosition::FirstRow)
		neighbor_indices = {my_neighbors[3], my_neighbors[4], my_neighbors[5], my_neighbors[6], my_neighbors[7]};
	if (condition == SquarePosition::LastRow)
		neighbor_indices = {my_neighbors[0], my_neighbors[1], my_neighbors[2], my_neighbors[3], my_neighbors[4]};
}
